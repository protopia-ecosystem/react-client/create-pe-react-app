#Protopia Ecosystem#

React клиент для Protopia Ecosystem

## Установка ##

1. Запустите команду `npx gitlab:protopiahome-public/protopia-ecosystem/react-client/create-pe-react-app`
1. Войдите в папку pe-app
2. Скопируйте из [node-server]/server/config/client/config.json в src/config/config.json react-клиента
3. запустите клиент командой:
    npm start
4. Войдите в систему под адресу localhost:3000/login
5. При необходите закоммитьте содержимое папки pe-app в своей репозиторий
