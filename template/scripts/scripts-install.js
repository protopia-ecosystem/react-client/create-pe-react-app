if (!require('fs').existsSync('pe-scripts')) {
    require('child_process').execSync(
        'git clone https://gitlab.com/protopiahome-public/protopia-ecosystem/react-client/pe-react-scripts pe-scripts',
        {stdio: 'inherit'}
    );
}
require('child_process').execSync(
    'node pe-scripts/install.js',
    {stdio: 'inherit'}
);