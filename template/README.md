#Protopia Ecosystem#

React клиент для Protopia Ecosystem

## Установка ##

1. Установите необходимые пакеты, выполнив в корневой директории команду:
    npm install
2. Скопируйте из [node-server]/server/config/client/config.json в src/config/config.json react-клиента
3. запустите клиент командой:
    npm start
4. Войдите в систему под адресу localhost:3000/login